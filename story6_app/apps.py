from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'story6_app'
