from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import kegiatanForm, membersForm
from .models import NewKegiatan,NewMembers

def kegiatan(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = kegiatanForm(request.POST or None)
        if form.is_valid():
            form.save()
            result = 1
        else:
            result = 0
        datakegiatan = NewKegiatan.objects.all()
        return render(request, 'kegiatan.html', {'form': form,'database':datakegiatan,'result':result})
    else:
        form = kegiatanForm()
        datakegiatan = NewKegiatan.objects.all()
        return render(request, 'kegiatan.html', {'form': form, 'database':datakegiatan})


def add_members(request):
    if request.method == 'POST':
        form = membersForm(request.POST)
        if form.is_valid():
            form.save()
            result = 1 
        else:
            result = 0
            datamember = NewMembers.objects.all()
        data_kegiatan = NewKegiatan.objects.all()
        data_member = NewMembers.objects.all()
        return render(request, 'add_members.html', {'form':form, 'result':result, 'datamember' : data_member, 'datakegiatan': data_kegiatan,})
    else:
        form = membersForm()
        data_kegiatan = NewKegiatan.objects.all()
        data_member = NewMembers.objects.all()
        return render(request,'add_members.html', {'form':form, 'datamember' : data_member, 'datakegiatan': data_kegiatan})
