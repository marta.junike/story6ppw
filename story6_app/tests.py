from django.test import TestCase
from django.test import Client
from .models import NewKegiatan,NewMembers
from .views import kegiatan,add_members
from .forms import kegiatanForm,membersForm
from django.http import HttpRequest

class memberTest(TestCase):
    def test_url_member_exist(self):
        response = Client().get("")
        self.assertEquals(response.status_code, 200)

    def test_member_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response,'add_members.html')

    def test_models_member_create(self):
        models_kegiatan = NewKegiatan.objects.create(nama_kegiatan="belajar")
        NewMembers.objects.create(Kegiatan = models_kegiatan, nama_anggota="dewi")
        jumlah = NewMembers.objects.all().count()
        self.assertEquals(jumlah, 1)

    def test_view_member_form(self):
        response = Client().get("")
        isi_html = response.content.decode("utf8")
        self.assertIn('>Daftar Kegiatan</', isi_html)
        self.assertIn('<form method="POST" action="" novalidate>', isi_html)

    def test_blank_input_members(self):
        data_kegiatan = NewKegiatan.objects.create(nama_kegiatan = "mancing")
        form = membersForm(data = {'Kegiatan' :data_kegiatan, 'nama_anggota' : ''})
        self.assertEqual(form.errors['nama_anggota'], ["This field is required."])

    def test_valid_post_method(self):
        response = self.client.post('',{'Kegiatan':'hehe','members':'dewi'})
        self.assertEqual(response.status_code, 200)

class kegiatanTest(TestCase):
    def test_url_kegiatan_exist(self):
        response = Client().get("/new_kegiatan/")
        self.assertEquals(response.status_code, 200)

    def test_kegiatan_template(self):
        response = Client().get("/new_kegiatan/")
        self.assertTemplateUsed(response,'kegiatan.html')

    def test_models_kegiatan_create(self):
        NewKegiatan.objects.create(nama_kegiatan="belajar")
        jumlah = NewKegiatan.objects.all().count()
        self.assertEquals(jumlah, 1)

    def test_view_kegiatan_form(self):
        response = Client().get("/new_kegiatan/")
        isi_html = response.content.decode("utf8")
        self.assertIn('>Tambah Kegiatan Baru</', isi_html)
        self.assertIn('<form method="POST" action="" novalidate>', isi_html)

    def test_valid_post_kegiatan_method(self):
        response = self.client.post('/new_kegiatan/',{'nama_kegiatan':'hehe'})
        self.assertEqual(response.status_code, 200)
        




