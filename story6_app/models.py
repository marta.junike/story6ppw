from django.db import models

# Create your models here.
class NewKegiatan(models.Model):
    nama_kegiatan = models.CharField("Nama Kegiatan", max_length=120, null=True, blank=False)
    def __str__(self):
        return f'{self.nama_kegiatan}'


class NewMembers(models.Model):
    Kegiatan = models.ForeignKey(NewKegiatan, on_delete=models.CASCADE, null=True, blank=False)
    nama_anggota = models.CharField("Nama Anggota", max_length=120, null=True, blank=False)
    def __str__(self):
        return f'{self.nama_anggota}'