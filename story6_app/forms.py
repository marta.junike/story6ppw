from django import forms
from .models import NewKegiatan,NewMembers

class kegiatanForm(forms.ModelForm):
    class Meta:
        model = NewKegiatan
        fields = '__all__'

class membersForm(forms.ModelForm):
    class Meta:
        model = NewMembers
        fields = '__all__'