from django.urls import path
from .views import kegiatan, add_members

app_name = 'story6_app'

urlpatterns = [
    path('', add_members, name='add_members'),
    path('new_kegiatan/',kegiatan, name='kegiatan'),
]
